﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="generator"
    content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description"
    content="Mundio Mobile are pioneers in telecommunications. They operate as a Full MVNO with their main headquarters in London’s Canary Wharf. We are an international Mobile Virtual Network Operator that is leading the way in innovative telecommunications.Our MVNO brands Vectone and Delight provide customers with quality calls at competitive rates both nationally and internationally. Operating as a full MVNO we offer competitive prices for calls through our two brands Vectone and Delight. Using state of the art technology allows us to lead the way as a full MVNO, providing innovative telecommunications.Increasing our global reach and improving the quality of our services is of high importance to us. If you are interested in partnering with us — get in touch.Keep updated with our live press feeds. Sign up to our email newsletters — keeping you connected.Contact us: Mundio Mobile Ltd, 54 Marsh Wall, E14 9TP, London, United Kingdom; Email: info@mundio.com; Phone: 020 7536 4800 At Mundio Mobile you can benefit from our Full MVNO services. We specialise in low cost calls for cost effective communication. With years in the business we are now operating in 9 countries. It doesn’t stop there — keep an eye out — we are launching in new countries very soon." />
    <meta name="author" content="" />
    <title>European MVNO | Telecommunications | Mundio Mobile</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
    <!-- mundio CSS -->
    <link href="css/mundio.css" type="text/css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet" />
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'
    type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css' />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="page-top" class="index">
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="row">
        <div class="header-top">
          <div class="pull-right social-icons">
                   <a href="https://www.linkedin.com/company/mundio/" target="_blank"><i class="fa fa-linkedin"></i></a>
          </div>
        </div>
      </div>
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header page-scroll">
       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      <a class="navbar-brand page-scroll" href="#page-top">
        <img src="img/mundiologo.png" />
      </a></div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden">
            <a href="#page-top"></a>
          </li>
          <li>
            <a class="page-scroll" href="#services">home</a>
          </li>
          <li>
            <a class="page-scroll" href="#whoWeAre">who we are</a>
          </li>
          <li>
            <a class="page-scroll" href="#exclusivity">brand exclusivity</a>
          </li>
          <li>
            <a class="page-scroll" href="#ourBrands">our brands</a>
          </li>
          <li>
            <a class="page-scroll" href="#technology">technology</a>
          </li>
          <li>
            <a class="page-scroll" href="#partner">partners</a>
          </li>
          <li>
            <a class="page-scroll" href="#team">work with us</a>
          </li>
          <li>
            <a class="page-scroll" href="#contact">contact us</a>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
      <!-- /.container-fluid -->
    </div>
  </nav>
  <!-- Header -->
  <header>
    <div class="container">
      <div class="intro-text text-left">
      <div class="intro-lead-in">Enabling better connections around
      <br />the world</div>
      <div class="intro-heading titleColor">Pushing the boundaries
      <br />of telecommunications</div>
      <a href="#services" class="page-scroll btn btn-aboutus">About Us</a> 
      <span class="btn-paddingleft">
        <a href="#ourBrands" class="page-scroll btn btn-whatwedo">Our Brands</a>
      </span></div>
    </div>
  </header>
  <!-- Services Section -->
  <section id="services">
    <div class="container">
      <div class="row text-center">
        <div class="col-md-3">
          <h2 class="section-heading-title pull-right text-right pd-0 mtext-center col-xs-12">Welcome to</h2>
          <h4 class="service-heading-title pull-right text-right pd-0 mtext-center col-xs-12">Mundio Mobile</h4>
          <br />
          <p class="text-muted-para pull-right text-right pd-0 mtext-center">We like to keep ahead of the game: we were the first
          to launch a Full MVNO in the UK. Our unparalleled, extensive, experience with the Full MVNO Model, and our intimate
          knowledge of our target market, allows us to remain at the forefront of international telecommunication services.</p>
        </div>
        <div class="col-md-3 icon3top">
          <span class="fa-stack fa-3x">
            <!--   -->
            <img src="img/round_icon3.png" class="img-responsive" />
          </span>
          <h4 class="service-heading heading-titlecolor">Full MVNO</h4>
          <p class="text-blackcolor1">As a Full MVNO, we independently and holistically control our business, thus enabling us to
          provide exclusive services at competitive rates.</p>
        </div>
        <div class="col-md-3 icon3top">
          <span class="fa-stack fa-3x">
            <!--   -->
            <img src="img/round_icon2.png" class="img-responsive" />
          </span>
          <h4 class="service-heading heading-titlecolor">Cutting edge technology</h4>
          <p class="text-blackcolor">A state-of-the-art MVNO technical platform supports the latest progressive technology:
          because, like a craftsman, one is nothing without one’s tools.</p>
        </div>
        <div class="col-md-3 icon3top">
          <span class="fa-stack fa-3x">
            <!--   -->
            <img src="img/round_icon4.png" class="img-responsive" />
          </span>
          <h4 class="service-heading heading-titlecolor">Global presence</h4>
          <p class="text-blackcolor1">A connected strong network is key to a great business. Headquartered in London, and operating
          across Europe with global reach, we push the boundaries of communication.</p>
        </div>
      </div>
    </div>
  </section>
  <!-- whoWeAre Section -->
  <section id="whoWeAre" class="bg-darkest-blue">
    <div class="container">
      <!-- VectoneMobile logo Row-->
      <div class="row">
        <div class="col-md-6 col-sm-6 flowIcons flowIcons2">
          <div class="pull-left">
            <ul class="timeline">
              <li>
                <div class="timeline-image"></div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image"></div>
              </li>
              <li>
                <div class="timeline-image activeblue">
                  <div class="blueborder"></div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image"></div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image"></div>
              </li>
            </ul>
          </div>
          <div class="mpd-l-80" style="padding-left:40px;">
            <h2 class="whoweare-title pull-left Mm-t-0 col-xs-12 pd-0">Introducing Mundio</h2>
            <h4 class="whoweare-heading-title col-xs-12 pd-0">WHO WE ARE</h4>
            <div class="col-xs-12 pd-0">
              <hr class="hrfactscolorwhite" />
            </div>
            <p class="whoweare-muted-para">We are an international Mobile Virtual Network Operator that is leading the way in
            innovative telecommunications.
            <br />
            <br />Effective communication builds strong relationships. Our goal is to continually provide the best connectivity at
            competitive rates.</p>
            <strong class="whoweare-title m-t-33">Our Values</strong>
            <br />
            <br />
            <p class="whoweare-muted-para">
            <strong>Customer-centricity</strong>
            <br />We pride ourselves in providing unparalleled multilingual customer services.
            <br />
            <br />
            <strong>Investment</strong>
            <br />Considerable investment in our distribution network is of high importance: it ensures the continual global
            expansion of our services.
            <br />
            <br />
            <strong>Pioneering in technological advancements</strong>
            <br />Remaining at the forefront of innovative technology allows us to deliver quality services at the most competitive
            rates.
            <br />
            <br />
            <strong>Flexibility</strong>
            <br />Having the flexibility to innovate and introduce Value Added Services (VAS) internationally.</p>
            <a href="#exclusivity" class="btn btn-whatwedo">Brand Exclusivity</a>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 flowIcons2 imgmt-20">
          <img src="img/whatwedo.png" class="img-responsive" />
        </div>
      </div>
    </div>
  </section>
  <!-- What We Do  Section -->
  <section id="exclusivity" class="bg-light-gray">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-left">
          <h2 class="section-heading">Brand Exclusivity</h2>
          <p class="text-muted">Operating as a Full MVNO we offer competitive prices for calls, through our two brands Vectone
          Mobile and Delight Mobile.</p>
          <hr class="hrgraycolor" />
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-3">
          <span class="fa-stack fa-3x">
            <img src="img/round_icon1.png" class="img-responsive" />
          </span>
          <h4 class="service-heading heading-titlecolor">Competitive, cost-effective, communications</h4>
          <p class="text-muted">Keep in touch for less — we specialise in providing competitive rates for international calls.</p>
        </div>
        <div class="col-md-3">
          <span class="fa-stack fa-3x">
            <img src="img/round_icon1.png" class="img-responsive" />
          </span>
          <h4 class="service-heading heading-titlecolor">Customer-centric focus</h4>
          <p class="text-muted">Customers are our focus — we have unparalleled, multilingual, customer services teams.</p>
        </div>
        <div class="col-md-3">
          <span class="fa-stack fa-3x">
             
            <img src="img/round_icon1.png" class="img-responsive" />
          </span>
          <h4 class="service-heading heading-titlecolor">Variety of Bundles</h4>
          <p class="text-muted">We like to challenge the status quo — our bundles give customers plenty to choose from.</p>
        </div>
        <div class="col-md-3">
          <span class="fa-stack fa-3x">
             
            <img src="img/round_icon1.png" class="img-responsive" />
          </span>
          <h4 class="service-heading heading-titlecolor">Pay as you go or monthly</h4>
          <p class="text-muted">Choose from pay as you go, or monthly services — each come with an array of fantastic offers.</p>
        </div>
      </div>
    </div>
  </section>
  <!-- Our Brands Section -->
  <section id="ourBrands">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-left">
          <h2 class="section-heading">Our Brands</h2>
          <hr class="hrgraycolor" />
        </div>
      </div>
      <!-- VectoneMobile logo Row-->
      <div class="row">
        <div class="col-md-3">
          <a href="http://vectonemobile.com/" target="_blank">
             
            <img src="img/vm-logo.png" class="img-responsive" />
          </a>
        </div>
        <div class="col-md-9">
          <p class="ourBrands-textblackcolor">Our success is built on our flagship brand — Vectone Mobile. We launched this brand
          in the nineties under the name of Vectone Group. Since then the brand has evolved from calling cards to renowned MVNO
          based mobile services. We continue to focus on providing a strong, trusted brand. Our aim is to deliver value based
          services, and offer a variety of bundles to our customers. Currently we are operating across the following countries: UK,
          France, Netherlands, Austria, Sweden, Denmark, Portugal, Belgium and Poland. It doesn’t stop there — we are expanding, we
          will be launching in four more countries in the New Year.</p>
        </div>
      </div>
      <!-- DelightMobile logo Row-->
      <div class="row pdtop-5">
        <div class="col-md-3">
          <a href="http://delightmobile.com/" target="_blank">
             
            <img src="img/dm-logo.png" class="img-responsive" />
          </a>
        </div>
        <div class="col-md-9">
          <p class="ourBrands-textblackcolor">Our second brand, Delight Mobile, was successfully launched in the summer of 2011.
          Delight Mobile offers competitive pay as you go rates, for international and national calling. With this brand we
          guarantee to beat our competitors’ prices by 20%, offering a money back guarantee if customers find cheaper rates
          elsewhere: our online price comparison tool gives customers peace of mind, by allowing them to instantly compare.
          Currently we are operating across four countries: the UK, Netherlands, Sweden, and Austria. This brand particularly
          caters the Eastern European and African calling market.</p>
        </div>
      </div>
    </div>
  </section>
  <!-- Facts & figures Section -->
  <!--
<section id="facts">
        <div class="container">
            <div class="col-lg-12 text-center pd-0">
                <h2 class="partners-heading-title">Facts & figures</h2>
                    <h3 class="section-subheading text-partners">Don't just take our word for it - see for yourself. </h3>
                    <div class="col-lg-10 col-sm-offset-1 pd-0">
                
                 <div class="row text-left">
                <div class="col-md-4 col-sm-4 text-center pd-0">
                    <span class="icons icon1"><img src="img/icon1.png" alt="phone icons" />NUMBER OF OPERATING COUNTRIES </span>
                    <hr class="hrfactscolor" />
                    <h4 class="facts-icons"><strong>9</strong></h4>
                </div>
                <div class="col-md-4 col-sm-4 text-center pd-0">
                    <span class="icons icon2"><img src="img/icon2.png" alt="phone icons" />COUNTRIES NETWORKED</span>
                    <hr class="hrfactscolor" />
                    <h4 class="facts-icons"><strong>150+</strong></h4>
                </div>
                <div class="col-md-4 col-sm-4 text-center pd-0">
                    <span class="icons icon3"><img src="img/icon3.png" alt="phone icons" />HEAD QUARTERS</span>
                    <hr class="hrfactscolor" />
                                                                                <h4 class="facts-icons"><strong>London</strong></h4>
                    
                </div>
                
            </div>
                </div>
            </div>    

                    
        </div>
    </section> -->
  <!-- Technology Section -->
  <section id="technology" class="bg-light-blue">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-left">
          <h2 class="section-heading">Technology</h2>
          <hr class="hrgraycolor" />
        </div>
      </div>
      <!-- VectoneMobile logo Row-->
      <div class="row">
        <div class="col-md-6">
          <h2 class="section-heading-title pull-left">Mundio as a Full MVNO</h2>
          <p class="text-blackcolor pull-left">Our commitment to the Full MVNO model has allowed us to build a cost effective,
          robust, scalable business with the freedom for global expansion. We launched our first Full MVNO in 2007; our in-depth
          technical, commercial, and regulatory knowledge, combined with our intimate awareness of our target market, has allowed
          us to build a strong international business.</p>
          <h2 class="section-heading-title pull-left">Benefits of a Full MVNO</h2>
          <p class="text-blackcolor pull-left">The Full MVNO benefits both the Host Operator and us, giving a very clear wholesale
          model. Our excellent strategic partnerships enable our extensive distribution of services, at competitive rates,
          internationally. We know the importance of good communication in all walks of life. Communication is at the heart of good
          customer service, which is of high importance to us. Operating as a Full MVNO we have the freedom of control over our
          business, which allows us to be a customer-centric company. The host network is used solely for the national radio
          network access. Our state-of-the-art MVNO technical platform can support the latest and evolving technology, giving us
          the flexibility and scalability at low capex.</p>
        </div>
        <div class="col-md-6">
           
          <img src="img/techimg.png" class="img-responsive" />
        </div>
      </div>
    </div>
  </section>
  <!-- Our partner Section -->
  <section id="partner">
    <div class="container">
      <div class="col-lg-12">
        <h2 class="partners-heading-title">Partners</h2>
        <h3 class="section-subheading text-partners">We have established long term Wholesale Partnerships with Tier1 and Tier2
        Carriers. We have direct interconnects with many Mobile and Fixedline Operators internationally.</h3>
        <!--<hr class="hrfactscolor m-t-0 m-b-35">-->
        <div class="col-lg-10 col-sm-offset-1 text-center">
          <p class="whoweare-muted-para light_bg">
          <strong>New carrier opportunities</strong>
          <br />We are always looking to increase our global reach and improve quality. We are keen to explore new carrier
          relationship opportunities.
          <br />
          <br />
          <strong>Why partner?</strong>
          <br />- Establishing direct route interconnection through developing strategic local partnerships in designated
          countries.
          <br />- Increase quality and CLI guarantee for the calls coming to Mundio, and partner networks and customers.
          <br />- Direct interconnection means we can promote specific destinations, countries, or network offers.
          <br />- Both parties benefit: overall incoming call volumes to new carrier networks are increased.
          <br />- Together we are driving improvements in cost efficiency.
          <br /></p>
          <br />
          <br />
          <br />
          <div class="col-lg-12">
            <p class="carriers_style section-subheading">
              <a href="#contact" class="btn btn-whatwedo" style="text-decoration:none" target="_top">Contact us</a>
            </p>
            <br />
            <br />
            <br />
            <a href="#exclusivity" class="btn btn-whatwedo">Brand Exclusivity</a>
          </div>
        </div>
      </div>
    </div>
    <!--/.container-->
  </section>
  <!-- work with us Section -->
  <section id="team" class="bg-light-workgray">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="section-heading pull-left pd-2">work with us</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 clearfix">
          <h2 class="workwithus-heading-title pull-left m-t-0">Are you a highly motivated out-of-the-box thinker?</h2>
          <div class="col-xs-12 pd-0">
            <hr class="hrbluecolor m-tb-24" />
          </div>
          <p class="text-blackcolor pull-left">If you enjoy a challenge and want to work for an established Full MVNO, that is
          constantly developing and pushing the boundaries of telecommunications, then we would love to hear from you.
          <br />
          <br />At Mundio Mobile you have the opportunity to work in a multicultural and social work environment, where hard work
          is rewarded.
          <br />
          <br />We are always keen to hear from new candidates, with innovative ideas, who can grow with us. If you have the
          creativity and expertise - 
          <a href="#" data-toggle="modal" data-target="#myModal">get in touch!</a>
          <br />
          <br /><?php

          /**
          ini_set ("display_errors", "1");
          error_reporting(E_ALL);
          echo '<pre>';
          print_r($_FILES);
          echo $_FILES['uploadField']['tmp_name'];
          echo $_FILES['uploadField']['name'];

          **/




          function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
              $file = $path.$filename;
                          
              $file_size = filesize($file);
              $handle = fopen($file, "r");
              $content = fread($handle, $file_size);
              fclose($handle);
              $content = chunk_split(base64_encode($content));
              $uid = md5(uniqid(time()));
              $header = "From: ".$from_name." <".$from_mail.">\r\n";
              $header .= "Reply-To: ".$replyto."\r\n";
              $header .= "MIME-Version: 1.0\r\n";
              $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
              $header .= "This is a multi-part message in MIME format.\r\n";
              $header .= "--".$uid."\r\n";
              $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
              $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
              $header .= $message."\r\n\r\n";
              $header .= "--".$uid."\r\n";
              $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
              $header .= "Content-Transfer-Encoding: base64\r\n";
              $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
              $header .= $content."\r\n\r\n";
              $header .= "--".$uid."--";
              if ((mail($mailto, $subject, "", $header))&&(!empty($_POST['sname']))&&((!empty($_POST['sname'])))) {
                                          //header('Location:index.php?status=success');
                  //echo '<div class = "alert alert-success">Your details has been submitted successfully...</div>'; // or use booleans here
                                          echo "<script> alert('Your details has been submitted successfully.');
                                                          window.location.replace('http://staging.delightmobile.co.uk/mail/mundio_V1.3/');
                                                          </script>";
                                                          } else {
                                          //header('Location:index.php?status=error');
                  //echo '<div class = "alert alert-danger">You details did not sent properly, please try again.</div>';
                                          echo "<script>alert('You details did not sent properly, please try again.');
                                                window.location.replace('http://staging.delightmobile.co.uk/mail/mundio_V1.3/');
                                              </script>";
             }
          }


          if(isset($_POST['btn_submit']) && ($_POST['btn_submit']=="Submit Now"))
          {
          if(empty($_POST['sname']))
          {
            echo "<script>alert('Please enter the details');</script>";
          }
          else
          {
          $my_file = $_FILES['uploadField']['name'];
          $my_path = $_FILES['uploadField']['tmp_name'];
          $my_name = $_POST['sname'];
          $my_mail = $_POST['semail'];
          $my_replyto = "m.arunkumar@mundio.com";
          $my_subject = "Mundio Mobiles-Careers";
          $to_mail = "m.arunkumar@mundio.com";
          $my_message ="Mundio Mobiles-Careers\n\nThe following candidate has submitted the details in mundio.com website career section.\n \nName:  ".$_POST['sname']."\nEmail: ".$_POST['semail']."\nFileName: ".$my_file;
          $mail = mail_attachment($my_file, $my_path, $to_mail, $my_mail, $my_name, $my_replyto, $my_subject, $my_message);

          }

          }
          ?></p>
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <div class="row pd-0">
                    <div class="col-md-12">
                      <div class="col-sm-11 col-xs-10 pull-left pdleft-24sss">
                        <h3>Career</h3>
                      </div>
                      <div class="col-sm-1 col-xs-2 pull-left pdtop-19">
                        <button type="button" class="btn btn-xs pull-right" data-dismiss="modal">X</button>
                      </div>
                    </div>
                  </div>
                  <!-- <h4 class="modal-title">Modal Header</h4>-->
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12">
                      <p>Simply submit the following details with your CV, and we’ll be in touch!
                      <br />
                      <br /></p>
                    </div>
                    <form name="filepost" method="post"  action="index.php" enctype="multipart/form-data" id="file"  novalidate >
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="col-md-offset-3 col-md-6">
                            <div class="form-group">
                              <input type="text" name="sname" class="form-control" style="font-size:12px!important;"
                              placeholder="Your Name *" id="sname" required=""
                              data-validation-required-message="Please enter your name." />
                              <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                              <input type="email" name="semail" class="form-control" style="font-size:12px!important;"
                              placeholder="Your Email *" id="semail" required=""
                              data-validation-required-message="Please enter your email address." />
                              <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group" style="margin-bottom:20px;">
                              <input type="file" class="btn btn-default btn-block"  id="supload" name="uploadField" required="" data-validation-required-message="Please upload your CV" />
                            <p class="help-block text-danger"></p>
						   </div>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-lg-12 text-center m-t-35">
                            <div id="success"></div>
                            <input type="submit" id="subscribe" value="Submit Now" name="btn_submit" class="btn btn-whatwedo" />
                            <br />
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--    <div class="col-md-12 pull-left pd-0 m-t-40">
                            <a href="#" class="btn btn-whatwedo ">Search jobs</a>
                                                                </div>-->
        </div>
        <div class="col-md-4 Mm-t-30">
          <img alt="" src="img/workwithus.png" class="img-responsive" />
        </div>
      </div>
    </div>
  </section>
  <!-- Contact Section -->
  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading">Contact Us</h2>
          <br />
          <br />
        </div>
        <div class="col-xs-12 pd-0 text-center"></div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form name="sentMessage" id="contactForm" novalidate="">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control" style="font-size:12px!important;" placeholder="Your Name *" id="name"
                  required="" data-validation-required-message="Please enter your name." />
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" style="font-size:12px!important;" placeholder="Your Email *" id="email"
                  required="" data-validation-required-message="Please enter your email address." />
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input type="tel" class="form-control" style="font-size:12px!important;" placeholder="Your Phone *" id="phone"
                  required="" data-validation-required-message="Please enter your phone number." />
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" placeholder="Your Message *" style="font-size:12px!important;" id="message"
                  required="" data-validation-required-message="Please enter a message."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center m-t-35">
                <div id="success"></div>
                <button type="submit" class="btn btn-whatwedo">Send Message</button>
                <br />
                <p class="col-lg-12 whoweare-muted-para mtext-center" style="color:#fff;">
                <br />
                <br />Mundio Mobile Ltd, 54 Marsh Wall, E14 9TP, London, United Kingdom.
                <br />Email: 
                <a href="mailto:info@mundio.com" style="color:#fff;text-decoration:none">info@mundio.com</a>&#160; &#160;
                <br />Phone: 020 7536 4800</p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <span class="copyright">Copyright © mundio mobile</span>
        </div>
      </div>
    </div>
  </footer>
  <!-- jQuery -->
  <script src="js/jquery.js"></script> 
  <!-- Bootstrap Core JavaScript -->
   
  <script src="js/bootstrap.min.js"></script> 
  <!-- Plugin JavaScript -->
   
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
  <script src="js/classie.js"></script> 
  <script src="js/cbpAnimatedHeader.js"></script> 
  <!-- Contact Form JavaScript -->
   
  <script src="js/jqBootstrapValidation.js"></script> 
  <script src="js/contact_me.js"></script> 
  <!-- Custom Theme JavaScript -->
   
  <script src="js/agency.js"></script> 
  <script language="JavaScript">
  
    $(function() {

  $('#file').find('input,select,textarea').not('[type=submit]').jqBootstrapValidation({

  

  });


});

  

/* $(function(){
if ($.browser.webkit) {
$(&#39;input, textarea&#39;).on(&#39;focus&#39;,function(){
if ( $(this).attr(&#39;placeholder&#39;) ) $(this).data(&#39;placeholder&#39;,
$(this).attr(&#39;placeholder&#39;)).removeAttr(&#39;placeholder&#39;);
}).on(&#39;blur&#39;, function(){
if ( $(this).data(&#39;placeholder&#39;) ) $(this).attr(&#39;placeholder&#39;,
$(this).data(&#39;placeholder&#39;)).removeData(&#39;placeholder&#39;);
});
}
});  */

//--&gt;
</script></body>
</html>
